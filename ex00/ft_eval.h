/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_eval.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pqueiroz <pqueiroz@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/06 12:31:25 by pqueiroz          #+#    #+#             */
/*   Updated: 2018/10/07 02:24:06 by pqueiroz         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_EVAL_H
# define FT_EVAL_H
# include "ft_lib.h"
# include "ft_string.h"

char	*ft_eval(char *str);
int		expr_has_prior(char *str, int prior);
int		ft_do_opr(int a, char opr, int b);
int		eval_expr(char *str);

#endif
