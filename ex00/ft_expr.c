/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_expr.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pqueiroz <pqueiroz@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/07 02:05:10 by pqueiroz          #+#    #+#             */
/*   Updated: 2018/10/07 02:25:27 by pqueiroz         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_eval.h"

int		expr_has_prior(char *str, int prior)
{
	int aux;
	int aux2;

	if (prior == 0)
		return (ft_str_indexof(str, '('));
	else if (prior == 1)
	{
		aux = ft_str_indexof(str, '*');
		aux2 = ft_str_indexof(str, '/');
		aux = (aux == -1 || (aux2 < aux && aux2 != -1)) ?
			aux2 : aux;
		aux2 = ft_str_indexof(str, '%');
		aux = (aux == -1 || (aux2 < aux && aux2 != -1)) ?
			aux2 : aux;
		return (aux);
	}
	else
	{
		aux = ft_str_indexof(str, '+');
		aux2 = ft_str_indexof(str, '-');
		aux = (aux == -1 || (aux2 < aux && aux2 != -1)) ?
			aux2 : aux;
		return (aux);
	}
}

int		ft_do_opr(int a, char opr, int b)
{
	if (opr == '+')
		return (a + b);
	else if (opr == '-')
		return (a - b);
	else if (opr == '*')
		return (a * b);
	else if (opr == '/')
		return (a / b);
	else
		return (a % b);
}

int		eval_expr(char *str)
{
	return (ft_atoi(ft_eval(ft_str_replace(str, " ", ""))));
}
