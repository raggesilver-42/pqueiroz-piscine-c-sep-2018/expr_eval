/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pqueiroz <pqueiroz@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/06 21:52:18 by pqueiroz          #+#    #+#             */
/*   Updated: 2018/10/07 02:08:12 by pqueiroz         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_lib.h"

char	g_list[16] = "0123456789ABCDEF";

int		ft_int_len_base(int a, int base)
{
	int i;

	i = 1;
	while (a / base)
	{
		a /= base;
		i++;
	}
	return (i);
}

long	ft_mod(long a)
{
	return ((a < 0) ? -a : a);
}

char	*ft_itoa_base(int val, int base)
{
	char	*res;
	int		i;
	int		len;
	int		ex;
	long	value;

	ex = (val < 0) ? 1 : 0;
	value = ft_mod((long)val);
	len = ft_int_len_base(value, base);
	res = malloc(sizeof(*res) * (len + 1 + ex));
	i = len + ex;
	res[i] = '\0';
	while (--i >= 0 + ex)
	{
		res[i] = g_list[value % base];
		value /= base;
	}
	if (ex == 1)
		res[0] = '-';
	return (res);
}

char	*ft_fk_strdup(char *dest, char *src)
{
	int i;

	i = 0;
	while (src[++i])
		dest[i] = src[i];
	dest[i] = 0;
	return (dest);
}

char	*ft_itoa(int value)
{
	return (ft_itoa_base(value, 10));
}
