/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_string.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pqueiroz <pqueiroz@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/06 11:21:57 by pqueiroz          #+#    #+#             */
/*   Updated: 2018/10/07 02:17:00 by pqueiroz         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_string.h"

int			ft_strlen(char *str)
{
	int i;

	i = 0;
	while (str[i])
		i++;
	return (i);
}

char		*ft_substr_s(char *src, int n, int start)
{
	char	*res;
	int		len;

	len = ft_strlen(src);
	n = (n < 0) ? len + n - 1 : n;
	start = (start < 0) ? len + start : start;
	if (n > len)
		return (src);
	len = n;
	res = malloc(sizeof(*res) * (len + 1));
	res[len] = 0;
	n = -1;
	while (++n < len)
		res[n] = src[start + n];
	return (res);
}

char		*ft_substr(char *src, int n)
{
	return (ft_substr_s(src, (n < 0) ? -n : n, (n < 0) ? n : 0));
}

int			ft_strcmp(char *s1, char *s2)
{
	while (*s1)
	{
		if (!*s2)
			return (1);
		if (*s1 != *s2)
			return (*s1 - *s2);
		s1++;
		s2++;
	}
	if (*s2)
		return (-1);
	return (0);
}

int			ft_str_indexof(char *str, char find)
{
	int i;

	i = -1;
	while (str[++i])
	{
		if (str[i] == find)
			return (i);
	}
	return (-1);
}
