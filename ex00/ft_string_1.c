/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_string_1.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pqueiroz <pqueiroz@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/06 11:21:57 by pqueiroz          #+#    #+#             */
/*   Updated: 2018/10/07 02:11:43 by pqueiroz         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_string.h"

int (*g_l)(char *) = &ft_strlen;

static char	*ft_str_replace_n2(char **aux, int oc, int i, int times)
{
	char *res;

	res = malloc(sizeof(*res) * i);
	res[i - 1] = '\0';
	i = 0;
	while (*aux[0])
	{
		oc = 0;
		if (ft_strcmp(ft_substr(aux[0], g_l(aux[1])), aux[1]) == 0
			&& (times > 0 || times == -1))
		{
			aux[0] = aux[0] + g_l(aux[1]);
			while (oc < g_l(aux[2]))
				res[i++] = aux[2][oc++];
			times--;
		}
		else
			res[i++] = *aux[0]++;
	}
	return (res);
}

char		*ft_str_replace_n(char *src, char *find, char *repl, int times)
{
	char	**aux;
	int		oc;
	int		i;

	i = 0;
	oc = 0;
	aux = malloc(sizeof(*aux) * 3);
	aux[0] = src;
	aux[1] = find;
	aux[2] = repl;
	while (src[i])
		i += (ft_strcmp(ft_substr(src + i, g_l(find)), find) == 0 && oc++)
			? g_l(find) : 1;
	oc = (times > -1) ? ft_number_roof(oc, times) : oc;
	i = (g_l(src) - (g_l(find) * oc) + g_l(repl) + 1);
	return (ft_str_replace_n2(aux, oc, i, times));
}

char		*ft_str_replace(char *src, char *find, char *repl)
{
	return (ft_str_replace_n(src, find, repl, -1));
}

char		*ft_strcat(char *s1, char *s2)
{
	int		i;
	char	*res;

	if (!s1)
		return (s2);
	if (!s2)
		return (s1);
	i = 0;
	res = malloc(sizeof(*res) * (ft_strlen(s1) + ft_strlen(s2) + 1));
	while (*s1)
		res[i++] = *s1++;
	while (*s2)
		res[i++] = *s2++;
	res[i] = 0;
	return (res);
}
